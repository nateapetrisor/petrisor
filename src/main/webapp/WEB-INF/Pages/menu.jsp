<%-- 
    Document   : menu
    Created on : Nov 5, 2019, 10:29:34 AM
    Author     : Petrisor
--%>
<div class="container">
 <nav class="navbar navbar-expand-md navbar-dark bg-dark">
  <a class="navbar-brand" t href="${pageContext.request.contextPath}">Parking lot</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
       <li class="nav-item ${activePage eq 'Cars' ? ' active' : ''}">
        <a class="nav-link" o href="${pageContext.request.contextPath}/Cars">Cars <span class="sr-only">(current)</span></a>
      </li>
       <li class="nav-item ${activePage eq 'User' ? ' active' : ''}">
        <a class="nav-link" o href="${pageContext.request.contextPath}/Users">Users <span class="sr-only">(current)</span></a>
      </li>
        <li class="nav-item ${pageContext.request.requestURI eq '/ParkingLot/About.jsp' ? ' active' : ''}">
        <a class="nav-link" o href="${pageContext.request.contextPath}/About.jsp">About <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="${pageContext.request.contextPath}/Login">Login</a>
        </li>
    </ul>
  </div>
</nav>
</div>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
    </body>
</html>
