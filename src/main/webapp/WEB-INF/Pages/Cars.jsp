<%-- 
    Document   : cars
    Created on : Nov 5, 2019, 11:08:50 AM
    Author     : Petrisor
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:pageTemplate pageTitle="Cars">
        <h1>Cars</h1>
        <c:forEach var="car" items="$cars" varStatus="status">
        <div class="row">
            <div class="col-md-4">
                ${car.licensePlate}
            </div>
            <div class="col-md-4">
                ${car.parkingSpot}
            </div>
            <div class="col-md-4">
                ${car.username}
            </div>
        </div>
    </c:forEach>
</t:pageTemplate>